package org.blog.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class LogBackValveApplication {
    public static void main(String[] args) {
        SpringApplication.run(LogBackValveApplication.class, args);
    }

    @RequestMapping("/server/info")
    public String getServerInfo() {
        return "server-info";
    }
}
